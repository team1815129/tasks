package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/go-telegram-bot-api/telegram-bot-api"
)

type UserStucture struct {
	Name   string
	ChatID int64
}

func main() {
	bot, err := tgbotapi.NewBotAPI("6694955555:AAEHozEjxXkHjK_OEwaoJymIFwEIvQgumtI")
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)
	n := 1

	file, _ := os.Create("message.txt")
	defer file.Close()

	for update := range updates {
		if update.Message == nil {
			continue
		}
		if update.Message.From.IsBot == false {
			file.WriteString(update.Message.Text + "\n")
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		if update.Message.IsCommand() {
			switch update.Message.Command() {
			case "start":
				n = 0
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Привет! Как тебя зовут?")
				bot.Send(msg)
				file.WriteString("Привет! Как тебя зовут?\n")

			default:
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Неправильная команда")
				bot.Send(msg)
				file.WriteString("Неправильная команда\n")
			}
		} else if update.Message.Text != "" && update.Message.IsCommand() == false && n == 0 {
			name := strings.TrimSpace(update.Message.Text)
			text := "Привет " + name + "!"
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, text)
			msg.ReplyToMessageID = update.Message.MessageID
			bot.Send(msg)
			file.WriteString(text + "\n")

			user := UserStucture{
				Name:   name,
				ChatID: update.Message.Chat.ID,
			}
			file.WriteString(fmt.Sprintf("User { FullName: %s, ChatID: %d }\n", user.Name, user.ChatID))

			n = 1
		} else {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Сперва напиши /start")
			msg.ReplyToMessageID = update.Message.MessageID
			bot.Send(msg)
			file.WriteString("Сперва напиши /start\n")
		}
	}
}
